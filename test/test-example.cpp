#include <string>
#include "gtest/gtest.h"
#include "../src/core-example.h"

TEST(TestSuiteName, TestCaseName)
{
    EXPECT_TRUE(functionExample(true));
    EXPECT_FALSE(functionExample(false));
}