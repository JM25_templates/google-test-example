#include <iostream>
#include "../src/core-example.h"
#include "config.h"

int main() {
    std::cout << "project name: " << PROJECT_NAME << " version: " << PROJECT_VER << std::endl;
    std::cout << "Hello, World! \n"
                << "Testing core lib function: " << std::boolalpha << functionExample(true) << std::endl;
    return 0;
}
